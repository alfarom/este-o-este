﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RulysiaMgr : MonoBehaviour {

    public RulysiaController[] rulysias;

    private SwipeController playerController;

    private float totalTime = 60.0f;
    private float nextAttack=3.0f;
    private float increaseDifficult=20.0f;
    private int numAdvices = 2;
    private int lastRulysia;
    private bool youWin;

    private void Start()
    {
        playerController = GameObject.FindGameObjectWithTag("Player").GetComponent<SwipeController>();
        youWin = false;
    }

    void Update () {

        totalTime -= Time.deltaTime;
        if (totalTime <= 0.0f && !youWin)
        {
            foreach (RulysiaController rulysiaController in rulysias)
            {
                rulysiaController.StopAllCoroutines();
                rulysiaController.enabled = false;
            }
            playerController.StartCoroutine("YouWin");
            youWin = true;
        }

        if (totalTime <= 0.0f) return;

        nextAttack -= Time.deltaTime;
        if (nextAttack <= 0.0f && totalTime>0.0f)
        {
            //Si quedan menos de 15 segundos mandamos dos coches
            if (totalTime < 15.0f)
            {
                //Si no hay dos coches moviendose y quedan
                //más de 3 segundos
                if (CarsMoving() < 2 && totalTime > 3.0f)
                {
                    StartCoroutine(SetDoubletAttack());
                }
                nextAttack = 0.5f;
            }
            else
            {
                StartCoroutine(SetNextAttack());
                nextAttack = 1f + numAdvices;
            }
           
        }

        increaseDifficult -= Time.deltaTime;
        if (increaseDifficult <= 0)
        {
            numAdvices--;
            if (numAdvices <= 0)
            {
                numAdvices = 1;
            }
            increaseDifficult = 20.0f;
        }

    }

    private bool IsSomeCarMoving()
    {
        foreach (RulysiaController rulusiaController in rulysias)
        {
            if (rulusiaController.isMoving)
                return true;
        }
        return false;
    }

    private int CarsMoving()
    {
        int result = 0;
        foreach (RulysiaController rulusiaController in rulysias)
        {
            if (rulusiaController.isMoving)
                result ++;
        }
        return result;
    }

    IEnumerator SetNextAttack()
    {
        //Sino mandamos un coche
        int numRulysia = GetNumRulysia();
        while (numRulysia == lastRulysia || rulysias[numRulysia].isMoving)
        {
            numRulysia = Random.Range(0, rulysias.Length);
            yield return new WaitForSeconds(0.1f);
        }
        lastRulysia = numRulysia;
        rulysias[numRulysia].StartCoroutine("Advice", numAdvices);
    }

    IEnumerator SetDoubletAttack()
    {

        int[] numRulysias = new int[2];
        int numTulysia = GetNumRulysia();

        while (rulysias[numTulysia].isMoving)
        {
            numTulysia = Random.Range(0, rulysias.Length);
            yield return new WaitForSeconds(0.1f);
        }
        numRulysias[0] = numTulysia;

        while (numRulysias[0] == numTulysia || rulysias[numTulysia].isMoving)
        {
            numTulysia = Random.Range(0, rulysias.Length);
            yield return new WaitForSeconds(0.1f);
        }
        numRulysias[1] = numTulysia;

        rulysias[numRulysias[0]].StartCoroutine("Advice", numAdvices);
        rulysias[numRulysias[1]].StartCoroutine("Advice", numAdvices);
    }

    private int GetNumRulysia()
    {
        int numRulysia = playerController.currentPos - 1;
        if (numRulysia > 2)
        {
            numRulysia = 2;
        }
        else if (numRulysia < 0)
        {
            numRulysia = 0;
        }

        return numRulysia;
    }
}
