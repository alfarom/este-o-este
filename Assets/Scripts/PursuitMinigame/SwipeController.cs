﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SwipeController : MonoBehaviour
{
    public FadeEffect fadeEffect;

    private const int MIN_POS = 1;
    private const int MAX_POS = 3;
    private const int MIN_LAYER = 4;
    private const int MAX_LAYER = 6;

    public int currentPos = 2;
    private Vector3 initPosition;
    private Vector3 finalPosition;
    private Coroutine coroutine;
    private SpriteRenderer spriteRenderer;
    private bool isLocked;
    private int lastPos=0;

    private void Start()
    {
        spriteRenderer = GetComponent<SpriteRenderer>();
    }

    // Update is called once per frame
    void Update() {

        if (isLocked) return;

        if (Input.GetMouseButtonDown(0))
        {
            initPosition = Input.mousePosition;
        }

        if (Input.GetMouseButtonUp(0))
        {
            if ((Input.mousePosition.y - initPosition.y) > 100)
            {
                spriteRenderer.sortingOrder -= 1;
                currentPos++;
                if (currentPos>MAX_POS)
                {
                    currentPos = MAX_POS;
                    spriteRenderer.sortingOrder = MIN_LAYER;
                }
                finalPosition = GetFinalPos(currentPos);

                if (lastPos != currentPos)
                {
                    if (coroutine != null)
                        StopCoroutine(coroutine);
                    coroutine = StartCoroutine("ChangePositionUp");
                    lastPos = currentPos;
                }
            }
            else if ((Input.mousePosition.y - initPosition.y) < -100)
            {
                spriteRenderer.sortingOrder += 1;
                currentPos--;
                if (currentPos < MIN_POS)
                {
                    currentPos = MIN_POS;
                    spriteRenderer.sortingOrder = MAX_LAYER;
                }
                finalPosition = GetFinalPos(currentPos);
                if (lastPos != currentPos)
                {
                    if (coroutine != null)
                        StopCoroutine(coroutine);
                    coroutine = StartCoroutine("ChangePositionDown");
                    lastPos = currentPos;
                }
            }
        }
    }

    private Vector3 GetFinalPos(int numPos)
    {
        switch (numPos)
        {
            case 1:
                return new Vector3(transform.position.x, -3, 0);
            case 2:
                return new Vector3(transform.position.x, -1.5f, 0);
            case 3:
                return new Vector3(transform.position.x, 0, 0);
        }
        return Vector3.zero;
    }

    IEnumerator YouWin()
    {
        isLocked = true;
        StopCoroutine("ChangePositionUp");
        StopCoroutine("ChangePositionDown");
        Vector3 initPosition = transform.position;
        Vector3 finalPosition = transform.position - (Vector3.right * 25);
        float deltaTime = 0.0f;
        bool exit = false;
        while (!exit)
        {
            deltaTime += Time.deltaTime * 3;
            transform.position = Vector3.Lerp(initPosition, finalPosition, deltaTime);
            yield return null;
            if (deltaTime > 1)
                exit = true;
        }
        Time.timeScale = 0.5f;
        fadeEffect.StartCoroutine("FadeIn", true);
    }

    IEnumerator ChangePositionUp()
    {
        Vector3 initPosition = transform.position;
        float deltaTime = 0.0f;
        while (transform.position.y!=finalPosition.y)
        {
            deltaTime += Time.deltaTime*3;
            transform.position = Vector3.Lerp(initPosition, finalPosition, deltaTime);
            yield return null;
        }

        transform.position = finalPosition;
        coroutine = null;
    }

    IEnumerator ChangePositionDown()
    {
        Vector3 initPosition = transform.position;
        float deltaTime = 0.0f;
        while ((initPosition.y - finalPosition.y) > 0.1f)
        {
            deltaTime += Time.deltaTime*3;
            transform.position = Vector3.Lerp(initPosition, finalPosition, deltaTime);
            yield return null;
        }

        transform.position = finalPosition;
        coroutine = null;
    }
}
