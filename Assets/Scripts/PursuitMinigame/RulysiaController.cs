﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RulysiaController : MonoBehaviour {

    public FadeEffect fadeEffect;
    private SpriteRenderer spriteRenderer;
    private Vector3 finalPosition, initPosition;
    private BoxCollider collider;
    public bool isMoving;

    private bool isReturning;
	// Use this for initialization
	void Start () {
        initPosition = transform.position;
        Transform playerTransform = GameObject.FindGameObjectWithTag("Player").transform;
        finalPosition = new Vector3(playerTransform.position.x, transform.position.y, transform.position.z);
        spriteRenderer = GetComponent<SpriteRenderer>();
        collider = GetComponent<BoxCollider>();
    }
	
    IEnumerator Advice(int numAdvices)
    {
        isMoving = true;
        for (int i = 0; i < 2; i++)
        {
            float deltaTime = 0;
            while (spriteRenderer.color.b > 0)
            {
                deltaTime += Time.deltaTime * (6 / numAdvices);
                spriteRenderer.color = Color.Lerp(Color.white, Color.red, deltaTime);
                yield return null;
            }

            deltaTime = 0;
            while (spriteRenderer.color.b <1)
            {
                deltaTime += Time.deltaTime * (6 / numAdvices);
                spriteRenderer.color = Color.Lerp(Color.red, Color.white, deltaTime);
                yield return null;
            }
            spriteRenderer.color = Color.white;
        }

        StartCoroutine(Attack());
    }

    IEnumerator Attack()
    {
        collider.enabled = true;
        Vector3 initPosition = transform.position;
        float deltaTime = 0.0f;
        while (finalPosition.x!= transform.position.x)
        {
            deltaTime += Time.deltaTime;
            transform.position = Vector3.Lerp(initPosition, finalPosition, deltaTime);
            yield return null;
        }

        StartCoroutine(Return());
    }

    IEnumerator Return()
    {
        collider.enabled = false;
        Vector3 finalPosition = transform.position;
        float deltaTime = 0.0f;
        while (transform.position.x!=initPosition.x)
        {
            deltaTime += Time.deltaTime/2;
            transform.position = Vector3.Lerp(finalPosition, initPosition, deltaTime);
            yield return null;
        }

        transform.position = initPosition;
        isMoving = false;
    }

    private void OnTriggerEnter(Collider collision)
    {
        if (collision.CompareTag("Player"))
        {
            collision.GetComponent<SwipeController>().enabled = false;
            StopAllCoroutines();
            Time.timeScale = 0.5f;
            fadeEffect.StartCoroutine("FadeIn",false);
        }
    }
}
