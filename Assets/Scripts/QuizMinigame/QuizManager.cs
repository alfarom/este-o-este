﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class QuizManager : MonoBehaviour {

    [System.Serializable]
    public class Question
    {
        [SerializeField]
        public bool answeredCorrectly;
        [SerializeField]
        public string questionText;
        [SerializeField]
        public int correctAnswer;
    };


    [SerializeField]
    string quizId = "Aliens";

    [SerializeField]
    Question [] questions;

    int currentAnswers = 0;

    [SerializeField]
    TextMeshProUGUI questionText;

    /// <summary>
    /// FOR TESTING PURPOSES
    /// </summary>
    [SerializeField]
    TextMeshProUGUI button1, button2;
    [SerializeField]
    GameObject titleScreen, basicTextScreen, questionScreen;

    public AudioClip sfxClip;


    void Start()
    {
        questionText.text = questions[currentAnswers].questionText;
        if(sfxClip!=null)
            GameMgr.Instance.GetServer<AudioMgr>().PlayFxOneShot(sfxClip);

    }
    
    public void CheckTheAnswer(int num)
    {
        questions[currentAnswers].answeredCorrectly = num == questions[currentAnswers].correctAnswer;
        currentAnswers++;
        if(quizId != "Zeke")
        {
            if (questions.Length == currentAnswers)
            {
                //Fin del Quiz
                CheckQuiz();
            }
            else
            {
                questionText.text = questions[currentAnswers].questionText;
            }
        }
        else
        {
            if(currentAnswers <= 2)
            {
                questionText.text = questions[currentAnswers].questionText;
            }
            else
            {
                if (currentAnswers == 3)
                    CheckQuiz();
                else
                {
                    if (currentAnswers == 5)
                    {
                        //Fin del Quiz
                        GameMgr.Instance.GetServer<SceneMgr>().ReturnScene(true);
                        GameMgr.Instance.GetEoEventsMgr().SetMiniGameSuccess(false);
                    }
                    else
                    {
                        questionText.text = questions[currentAnswers].questionText;
                        button1.text = "Aliño de vinagre balsámico de módena";
                        button2.text = "Ketchup";
                    }

                }
            }
        }
        if (sfxClip != null)
            GameMgr.Instance.GetServer<AudioMgr>().PlayFxOneShot(sfxClip);
    }

    void CheckQuiz()
    {
        switch(quizId)
        {
            case "Aliens":
            case "CharlieBoy":
                GameMgr.Instance.GetServer<SceneMgr>().ReturnScene(true);
                if (questions[2].answeredCorrectly)
                {
                    //Opción correcta
                    GameMgr.Instance.GetEoEventsMgr().SetMiniGameSuccess(true);
                }
                else
                {
                    //Opción no correcta
                    GameMgr.Instance.GetEoEventsMgr().SetMiniGameSuccess(false);
                }
                
                break;
            case "Police":
                //Si fallas una pregunta pierdes
                foreach (Question q in questions)
                {
                    if (!q.answeredCorrectly)
                    {
                        GameMgr.Instance.GetServer<SceneMgr>().ReturnScene(true);
                        GameMgr.Instance.GetEoEventsMgr().SetMiniGameSuccess(false);
                        return;
                    } 
                }
                GameMgr.Instance.GetServer<SceneMgr>().ReturnScene(true);
                GameMgr.Instance.GetEoEventsMgr().SetMiniGameSuccess(true);
                break;
            case "Normal":
                //Si fallas las ultima pregunta tienes un final y si aciertas otro.
                GameMgr.Instance.GetServer<SceneMgr>().ReturnScene(true);
                if (questions[2].answeredCorrectly)
                {
                    GameMgr.Instance.GetEoEventsMgr().SetMiniGameSuccess(true);
                }
                else
                {
                    GameMgr.Instance.GetEoEventsMgr().SetMiniGameSuccess(false);
                }
                break;
            case "Zeke":
                if(currentAnswers == 3)
                {
                    if (questions[2].answeredCorrectly)
                    {
                        GameMgr.Instance.GetServer<SceneMgr>().ReturnScene(true);
                        GameMgr.Instance.GetEoEventsMgr().SetMiniGameSuccess(true);
                    }
                    else
                    {
                        questionText.text = questions[currentAnswers].questionText;
                        button1.text = "Puré de patatas";
                        button2.text = "Arroz";
                    }
                }
                break;
            default:
                break;
        }
    }

}
