﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class FadeEffect : MonoBehaviour {

    public Image image;

    IEnumerator FadeIn(bool success)
    {
        float deltaTime = 0;
        while (image.color.a < 1)
        {
            deltaTime += Time.deltaTime;
            image.color = Color.Lerp(Color.clear, Color.white, deltaTime);
            yield return null;
        }
        Time.timeScale = 1f;
        GameMgr.Instance.GetServer<SceneMgr>().ReturnScene(true);
        GameMgr.Instance.GetEoEventsMgr().SetMiniGameSuccess(success);
    }

    IEnumerator FadeOutBlack()
    {
        float deltaTime = 0;
        image.color = Color.black;
        while (image.color.a > 0)
        {
            deltaTime += Time.deltaTime;
            image.color = Color.Lerp(Color.black, Color.clear, deltaTime);
            yield return null;
        }
        Time.timeScale = 1f;
    }
}
