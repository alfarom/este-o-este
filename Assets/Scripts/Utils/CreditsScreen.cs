﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CreditsScreen : MonoBehaviour {

    public GameObject backButton;

	void OnEnable () {
        backButton.SetActive(false);
        StartCoroutine("CloseScreen");
	}
	
    IEnumerator CloseScreen()
    {
        yield return new WaitForSeconds(3.2f);
        backButton.SetActive(true);
    }

}
