﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ButtonMinigame : MonoBehaviour {

    public void WinMiniGame()
    {
        GameMgr.Instance.GetEoEventsMgr().SetMiniGameSuccess(true);
        GameMgr.Instance.GetServer<SceneMgr>().ReturnScene(true);
    }

    public void LoseMiniGame()
    {
        GameMgr.Instance.GetEoEventsMgr().SetMiniGameSuccess(false);
        GameMgr.Instance.GetServer<SceneMgr>().ReturnScene(true);
    }
}
