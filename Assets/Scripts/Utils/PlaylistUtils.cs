﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlaylistUtils : MonoBehaviour {

    public AudioClip[] clips;
    public AudioClip sfxClip;
    private int numClip;

	// Use this for initialization
	void Start () {
        if(clips.Length>1)
            GameMgr.Instance.GetServer<AudioMgr>().PlayOneShot(clips[numClip++]);
        GameMgr.Instance.GetServer<AudioMgr>().PlayFX(sfxClip);
        GameMgr.Instance.GetServer<AudioMgr>().StopPlay();
    }
	
	// Update is called once per frame
	void Update () {
        if(clips.Length>0)
        if (!GameMgr.Instance.GetServer<AudioMgr>().isPlaying())
        {
            GameMgr.Instance.GetServer<AudioMgr>().Play(clips[numClip++]);

            if (numClip == clips.Length)
            {
                numClip--;
            }
        }
	}

    void OnDestroy()
    {
        GameMgr.Instance.GetServer<AudioMgr>().StopPlayFX();
    }
}
