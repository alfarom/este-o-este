﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayMusicOnStart : MonoBehaviour {

    public AudioClip clip;

	void Start () {
        if (clip != null)
            GameMgr.Instance.GetServer<AudioMgr>().Play(clip);
        else
            GameMgr.Instance.GetServer<AudioMgr>().StopPlay();

        

    }

}
