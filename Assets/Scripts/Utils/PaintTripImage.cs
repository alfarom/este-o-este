﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PaintTripImage : MonoBehaviour {

    public Image image;
    public GameObject[] eventsImages;
    private float fillAmount;
    
    public void SetFillAmount(float fillAmount, string nameImage)
    {
        this.fillAmount = fillAmount;
        StartCoroutine(FillImage(nameImage));
    }

    IEnumerator FillImage(string nameImage)
    {

        while ((fillAmount - image.fillAmount)>0.1f)
        {
            image.fillAmount += 0.045f;
           yield return new WaitForSeconds(0.5f);
        }

        image.fillAmount = fillAmount;

        GameObject eventImage = GetEventImage(nameImage);
        if (eventImage != null)
        {
            eventImage.SetActive(true);
        }

        yield return new WaitForSeconds(2f);

        GameMgr.Instance.GetEoEventsMgr().EndAdvanceScene();
    }

    private GameObject GetEventImage(string nameImage)
    {

        foreach(GameObject image in eventsImages)
        {
            if (image.name.Equals(nameImage))
            {
                return image;
            }
        }
        return null;
    }
}
