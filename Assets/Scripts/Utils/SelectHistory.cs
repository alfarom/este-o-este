﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SelectHistory : MonoBehaviour {

    public EoEvent history;

	public void SetHistory()
    {
        GameMgr.Instance.InitialEoEvent = history;
    }
}
