﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RetryButton : MonoBehaviour {

    public void Retry()
    {

        GameMgr.Instance.GetServer<SceneMgr>().ChangeScene("MapScene");
    }
}
