﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CarUI : MonoBehaviour {

    public Vector3 initPosition, finalPosition;
	
	void Start () {
        StartCoroutine(Move());
	}
	
	IEnumerator Move()
    {
        float deltaTime = 0.0f;
        while (transform.position.x != finalPosition.x)
        {
            deltaTime += Time.deltaTime / 60;
            transform.position = Vector3.Lerp(initPosition, finalPosition, deltaTime);
            yield return null;
        }
    }
}
