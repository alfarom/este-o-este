﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TransitionController : MonoBehaviour {

    public Image image;
    public FadeEffect fadeEffect;

    public void SetAction(EoEvent eoEvent)
    {

        TransitionEoEvent transitonEoEvent = (TransitionEoEvent)eoEvent;
        image.sprite = eoEvent.image;
        if (transitonEoEvent.sfxClip != null)
        {
            GameMgr.Instance.GetServer<AudioMgr>().PlayFxOneShot(transitonEoEvent.sfxClip);
        }

        if (transitonEoEvent.clip != null)
        {
            GameMgr.Instance.GetServer<AudioMgr>().Play(transitonEoEvent.clip);
        }

        if (transitonEoEvent.fadeTransition)
        {
            fadeEffect.StartCoroutine("FadeOutBlack");
        }
        StartCoroutine(Wait(transitonEoEvent.duration));
    }

    IEnumerator Wait(float time)
    {

        yield return new WaitForSeconds(time);
        GameMgr.Instance.GetEoEventsMgr().EndTransitionScene();
    }
}
