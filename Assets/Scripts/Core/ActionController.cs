﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class ActionController : MonoBehaviour {

    public Text text, option1Text, option2Text;
    public TextMeshProUGUI textM, option1TextM, option2TextM;
    public Image image;

    public void SetAction(EoEvent eoEvent)
    {
        textM.text = eoEvent.text;
        image.sprite = eoEvent.image;
        option1TextM.text = eoEvent.textOption1;
        if (eoEvent.GetType() == typeof(UseObjectEoEvent))
        {
            UseObjectEoEvent useEvent = (UseObjectEoEvent)eoEvent;
            if (!PlayerPrefs.HasKey(useEvent.key))
            {
                //Si no la tiene mostramos 
                option1Text.text = useEvent.optionText;
                option2Text.transform.parent.gameObject.SetActive(false);
                return;
            }
        }

        option1TextM.text = eoEvent.textOption1;
        if (eoEvent.option2 != null)
        {
            option2TextM.transform.parent.parent.gameObject.SetActive(true);
            option2TextM.text = eoEvent.textOption2;
        }
        else
            option2TextM.transform.parent.parent.gameObject.SetActive(false);

        GameMgr.Instance.GetServer<AudioMgr>().PlayFxOneShot(eoEvent.sfxClip);
    }
}
