﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameMgr : MonoBehaviour {

    #region Gestion del singleton

    private static GameMgr _instance;
    private GameObject m_servers = null;
    public EoEventsMgr eoEventsMgr;

    public static GameMgr Instance
    {
        get
        {
            if (_instance == null)
            {
                Debug.Log("GameMgr no inicializado.");
                return null;
            }
            return _instance;
        }
    }

    void Awake()
    {
        if (_instance != null)
        {
            Debug.Log("Hay dos componentes en la escena, se elimina el nuevo");
            Destroy(this);
            return;
        }
        _instance = this;
        m_servers = gameObject;
        SceneMgr sceneMgr = AddServer<SceneMgr>();
        AudioMgr audioMgr = AddServer<AudioMgr>();
        AchievementMgr achivementMgr = AddServer<AchievementMgr>();


        if (Debug.isDebugBuild)
        {

        }

        DontDestroyOnLoad(gameObject);
    }

    void OnDestroy()
    {
        if (_instance == this)
            _instance = null;
        Destroy(gameObject);
    }

    #endregion

    public EoEvent InitialEoEvent { get; set; }

    protected T AddServer<T>() where T : Component
    {
        T t = m_servers.GetComponent<T>();
        if (t != null)
            Component.DestroyImmediate(t);
        t = m_servers.AddComponent<T>();
        return t;
    }

    public T GetServer<T>() where T : Component
    {
        if (m_servers)
            return m_servers.GetComponent<T>();
        else
            return null;
    }

    public void SetEoEventsMgr(EoEventsMgr eoEventsMgr)
    {
        this.eoEventsMgr = eoEventsMgr;
    }

    public EoEventsMgr GetEoEventsMgr()
    {
        return eoEventsMgr;
    }

}
