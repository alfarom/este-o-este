﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class NPCController : MonoBehaviour {

    public TextMeshProUGUI text, option1Text, option2Text;
    public Image image;


    public void SetAction(EoEvent eoEvent)
    {
        text.text = eoEvent.text;
        image.sprite = eoEvent.image;

        if (eoEvent.GetType() == typeof(UseObjectEoEvent))
        {
            UseObjectEoEvent useEvent = (UseObjectEoEvent)eoEvent;
            if (!PlayerPrefs.HasKey(useEvent.key))
            {
                //Si no la tiene mostramos 
                option1Text.text = useEvent.optionText;
                option2Text.transform.parent.gameObject.SetActive(false);
                return;
            }
        }
        
        option1Text.text = eoEvent.textOption1;
        if (eoEvent.option2 != null)
        {
            option2Text.transform.parent.parent.gameObject.SetActive(true);
            option2Text.text = eoEvent.textOption2;
        }
        else
            option2Text.transform.parent.parent.gameObject.SetActive(false);

        GameMgr.Instance.GetServer<AudioMgr>().PlayFxOneShot(eoEvent.sfxClip);
    }
}
