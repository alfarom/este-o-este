﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class GameOverController : MonoBehaviour {

    public TextMeshProUGUI text, option1Text;
    public Image image;


    public void SetAction(EoEvent eoEvent)
    {
        text.text = eoEvent.text;
        image.sprite = eoEvent.image;
        option1Text.text = eoEvent.textOption1;
    }
}
