﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class EoEventsMgr : MonoBehaviour {

    public NPCController npcController;
    public ActionController actionController;
    public GameOverController gameOverController;
    public AngelController angelController;
    public TransitionController transitionController;

    public PaintTripImage paintTripImage;
    public EoEvent currentAction;

    private List<string> objectKeys = new List<string>();

    public void Start()
    {
        GameMgr.Instance.SetEoEventsMgr(this);
        if (GameMgr.Instance.InitialEoEvent!=null)
            currentAction = GameMgr.Instance.InitialEoEvent;
        PlayerPrefs.SetInt("Angel", 0);
        StartNextEoEvent();
    }

    public void OnDestroy()
    {
        foreach(string key in objectKeys)
        {
            PlayerPrefs.DeleteKey(key);
        }
        objectKeys.Clear();
    }

    public void SetAction(EoEvent action)
    {
        this.currentAction = action;
    }

    public void AcceptAction()
    {
        //Paramos los sonidos de FX
        GameMgr.Instance.GetServer<AudioMgr>().StopPlayFX();
        if (currentAction.GetType() == typeof(GameOverEoEvent))
        {
            SceneManager.LoadScene(((GameOverEoEvent)currentAction).nameScene);
            return;
        }
        else if (currentAction.GetType() == typeof(AdvanceGameOverEoEvent))
        {
            SceneManager.LoadScene(((AdvanceGameOverEoEvent)currentAction).nameScene);
            return;
        }
        else if (currentAction.GetType() == typeof(AngelEoEvent))
        {
            if (PlayerPrefs.GetInt("Angel") == 1)
            {
                PlayerPrefs.DeleteKey("Angel");
                GameMgr.Instance.GetServer<SceneMgr>().PushScene(((MinigameEoEvent)currentAction.option1).minigameScene);
                currentAction = ((AngelEoEvent)currentAction).option1;
                return;
            }
            else if (PlayerPrefs.HasKey("Angel"))
            {
                PlayerPrefs.SetInt("Angel", 1);
            }
        }
        else if (currentAction.GetType() == typeof(FindObjectEoEvent))
        {
            FindObjectEoEvent findObjectEoEvent = (FindObjectEoEvent)currentAction;
            PlayerPrefs.SetInt(findObjectEoEvent.key, 1);
            objectKeys.Add(findObjectEoEvent.key);
        }

        //Si es un evento de FindObject
        if (currentAction.option1.findObjectEvent!=null && currentAction.option1.findObjectEvent.GetType() == typeof(FindObjectEoEvent))
        {
            FindObjectEoEvent findObjectEoEvent = (FindObjectEoEvent)currentAction.option1.findObjectEvent;

            //Si ya hemos cogido el objeto ponemos la siguiente esccena
            if (PlayerPrefs.HasKey(findObjectEoEvent.key))
            {
                currentAction = currentAction.option1;
                StartNextEoEvent();
                return;
            }
            else
            {
                if (currentAction.option1.findObjectEvent.isNPCEvent)
                {
                    angelController.gameObject.SetActive(false);
                    gameOverController.gameObject.SetActive(false);
                    actionController.gameObject.SetActive(false);
                    transitionController.gameObject.SetActive(false);
                    npcController.gameObject.SetActive(true);
                    npcController.SetAction(currentAction.option1.findObjectEvent);
                }
                else
                {
                    angelController.gameObject.SetActive(false);
                    gameOverController.gameObject.SetActive(false);
                    npcController.gameObject.SetActive(false);
                    transitionController.gameObject.SetActive(false);
                    actionController.gameObject.SetActive(true);
                    actionController.SetAction(currentAction.option1.findObjectEvent);
                }
                currentAction = currentAction.option1.findObjectEvent;
                return;
            }
        }

        if (currentAction.option1.GetType() == typeof(AdvanceEoEvent))
        {
            npcController.gameObject.SetActive(false);
            actionController.gameObject.SetActive(false);
            gameOverController.gameObject.SetActive(false);
            angelController.gameObject.SetActive(false);
            transitionController.gameObject.SetActive(false);
            //Pintar el camino
            AdvanceEoEvent advEvent = (AdvanceEoEvent)currentAction.option1;
            paintTripImage.SetFillAmount(advEvent.distance, advEvent.activeImage);
        }
        else if (currentAction.option1.GetType() == typeof(MinigameEoEvent))
        {
            //Lanzar el minijuego y esperar a ver el resultado
            GameMgr.Instance.GetServer<SceneMgr>().PushScene(((MinigameEoEvent)currentAction.option1).minigameScene);

            //SceneManager.LoadScene(((MinigameEoEvent)currentAction.option2).minigameScene);
        }
        else if (currentAction.option1.GetType() == typeof(GameOverEoEvent))
        {
            if (PlayerPrefs.HasKey("Angel"))
            {
                GameOverEoEvent goEoEvent = (GameOverEoEvent)currentAction.option1;
                if (PlayerPrefs.GetInt("Angel") == 1 && goEoEvent.angelEvent!=null)
                {
                    npcController.gameObject.SetActive(false);
                    actionController.gameObject.SetActive(false);
                    gameOverController.gameObject.SetActive(false);
                    transitionController.gameObject.SetActive(false);
                    angelController.gameObject.SetActive(true);
                    angelController.SetAction(goEoEvent.angelEvent);
                    currentAction = goEoEvent.angelEvent;
                    return;
                }
            }
            //Lanzar escena fin del juego
            npcController.gameObject.SetActive(false);
            actionController.gameObject.SetActive(false);
            angelController.gameObject.SetActive(false);
            transitionController.gameObject.SetActive(false);
            gameOverController.gameObject.SetActive(true);
            gameOverController.SetAction(currentAction.option1);
        }
        else if (currentAction.option1.GetType() == typeof(AdvanceGameOverEoEvent))
        {
            npcController.gameObject.SetActive(false);
            actionController.gameObject.SetActive(false);
            gameOverController.gameObject.SetActive(false);
            angelController.gameObject.SetActive(false);
            transitionController.gameObject.SetActive(false);
            //Pintar el camino
            AdvanceGameOverEoEvent advEvent = (AdvanceGameOverEoEvent)currentAction.option1;
            paintTripImage.SetFillAmount(advEvent.distance, advEvent.activeImage);
        }
        else if (currentAction.option1.GetType() == typeof(TransitionEoEvent))
        {
            npcController.gameObject.SetActive(false);
            actionController.gameObject.SetActive(false);
            gameOverController.gameObject.SetActive(false);
            angelController.gameObject.SetActive(false);
            transitionController.gameObject.SetActive(true);
            transitionController.SetAction(currentAction.option1);
        }
        else if (currentAction.option1.GetType() == typeof(UseObjectEoEvent))
        {
            UseObjectEoEvent useEvent = (UseObjectEoEvent)currentAction.option1;
            if (!PlayerPrefs.HasKey(useEvent.key))
            {
                currentAction = useEvent.alternativeEvent;
                StartNextEoEvent();
                return;
            }
            else
            {
                currentAction = useEvent.option1;
                StartNextEoEvent();
                return;
            }
        }
        else
        {
            if (currentAction.option1.isNPCEvent)
            {
                angelController.gameObject.SetActive(false);
                gameOverController.gameObject.SetActive(false);
                actionController.gameObject.SetActive(false);
                transitionController.gameObject.SetActive(false);
                npcController.gameObject.SetActive(true);
                npcController.SetAction(currentAction.option1);
            }
            else
            {
                angelController.gameObject.SetActive(false);
                gameOverController.gameObject.SetActive(false);
                npcController.gameObject.SetActive(false);
                transitionController.gameObject.SetActive(false);
                actionController.gameObject.SetActive(true);
                actionController.SetAction(currentAction.option1);
            }
        }
        currentAction = currentAction.option1;
        if(currentAction.clip!=null)
            GameMgr.Instance.GetServer<AudioMgr>().Play(currentAction.clip);
    }
    
    public void DeclineAction()
    {
        //Paramos los sonidos de FX
        GameMgr.Instance.GetServer<AudioMgr>().StopPlayFX();

        //Si es un evento de FindObject
        if (currentAction.option2.findObjectEvent != null && currentAction.option2.findObjectEvent.GetType() == typeof(FindObjectEoEvent))
        {
            FindObjectEoEvent findObjectEoEvent = (FindObjectEoEvent)currentAction.option2.findObjectEvent;

            //Si ya hemos cogido el objeto ponemos la siguiente esccena
            if (PlayerPrefs.HasKey(findObjectEoEvent.key))
            {
                currentAction = currentAction.option2;
                StartNextEoEvent();
                return;
            }
            else
            {
                if (currentAction.option2.findObjectEvent.isNPCEvent)
                {
                    angelController.gameObject.SetActive(false);
                    gameOverController.gameObject.SetActive(false);
                    actionController.gameObject.SetActive(false);
                    transitionController.gameObject.SetActive(false);
                    npcController.gameObject.SetActive(true);
                    npcController.SetAction(currentAction.option2.findObjectEvent);
                }
                else
                {
                    angelController.gameObject.SetActive(false);
                    gameOverController.gameObject.SetActive(false);
                    npcController.gameObject.SetActive(false);
                    transitionController.gameObject.SetActive(false);
                    actionController.gameObject.SetActive(true);
                    actionController.SetAction(currentAction.option2.findObjectEvent);
                }
                currentAction = currentAction.option2.findObjectEvent;
                return;
            }
        }

        if (currentAction.option2.GetType() == typeof(AdvanceEoEvent))
        {
            npcController.gameObject.SetActive(false);
            actionController.gameObject.SetActive(false);
            gameOverController.gameObject.SetActive(false);
            angelController.gameObject.SetActive(false);
            transitionController.gameObject.SetActive(false);
            //Pintar el camino
            AdvanceEoEvent advEvent = (AdvanceEoEvent)currentAction.option2;
            paintTripImage.SetFillAmount(advEvent.distance, advEvent.activeImage);
        }
        else if (currentAction.option2.GetType() == typeof(MinigameEoEvent))
        {
            //Lanzar el minijuego y esperar a ver el resultado
            GameMgr.Instance.GetServer<SceneMgr>().PushScene(((MinigameEoEvent)currentAction.option2).minigameScene);
            //SceneManager.LoadScene(((MinigameEoEvent)currentAction.option2).minigameScene);
        }
        else if (currentAction.option2.GetType() == typeof(GameOverEoEvent))
        {
            if (PlayerPrefs.HasKey("Angel"))
            {
                GameOverEoEvent goEoEvent = (GameOverEoEvent)currentAction.option2;
                if (PlayerPrefs.GetInt("Angel") == 1 && goEoEvent.angelEvent != null)
                {
                    npcController.gameObject.SetActive(false);
                    actionController.gameObject.SetActive(false);
                    gameOverController.gameObject.SetActive(false);
                    transitionController.gameObject.SetActive(false);
                    angelController.gameObject.SetActive(true);
                    angelController.SetAction(goEoEvent.angelEvent);
                    currentAction = goEoEvent.angelEvent;
                    return;
                }
            }
            //TODO Lanzar el minijuego y esperar a ver el resultado
            npcController.gameObject.SetActive(false);
            actionController.gameObject.SetActive(false);
            angelController.gameObject.SetActive(false);
            transitionController.gameObject.SetActive(false);
            gameOverController.gameObject.SetActive(true);
            gameOverController.SetAction(currentAction.option2);
        }
        else if (currentAction.option2.GetType() == typeof(AdvanceGameOverEoEvent))
        {
            npcController.gameObject.SetActive(false);
            actionController.gameObject.SetActive(false);
            gameOverController.gameObject.SetActive(false);
            angelController.gameObject.SetActive(false);
            transitionController.gameObject.SetActive(false);
            //Pintar el camino
            AdvanceGameOverEoEvent advEvent = (AdvanceGameOverEoEvent)currentAction.option2;
            paintTripImage.SetFillAmount(advEvent.distance, advEvent.activeImage);
        }
        else if (currentAction.option2.GetType() == typeof(TransitionEoEvent))
        {
            npcController.gameObject.SetActive(false);
            actionController.gameObject.SetActive(false);
            gameOverController.gameObject.SetActive(false);
            angelController.gameObject.SetActive(false);
            transitionController.gameObject.SetActive(true);
            transitionController.SetAction(currentAction.option1);
        }
        else if (currentAction.option2.GetType() == typeof(UseObjectEoEvent))
        {
            UseObjectEoEvent useEvent = (UseObjectEoEvent)currentAction.option2;
            if (!PlayerPrefs.HasKey(useEvent.key))
            {
                currentAction = useEvent.alternativeEvent;
                StartNextEoEvent();
                return;
            }
            else
            {
                currentAction = useEvent.option1;
                StartNextEoEvent();
                return;
            }
        }
        else
        {
            if (currentAction.option2.isNPCEvent)
            {
                angelController.gameObject.SetActive(false);
                actionController.gameObject.SetActive(false);
                gameOverController.gameObject.SetActive(false);
                transitionController.gameObject.SetActive(false);
                npcController.gameObject.SetActive(true);
                npcController.SetAction(currentAction.option2);
            }
            else
            {
                angelController.gameObject.SetActive(false);
                npcController.gameObject.SetActive(false);
                gameOverController.gameObject.SetActive(false);
                transitionController.gameObject.SetActive(false);
                actionController.gameObject.SetActive(true);
                actionController.SetAction(currentAction.option2);
            }
        }
        currentAction = currentAction.option2;
        if (currentAction.clip != null)
            GameMgr.Instance.GetServer<AudioMgr>().Play(currentAction.clip);
    }


    public void SetMiniGameSuccess(bool success)
    {
        if (success)
        {
            //Si ganamos es como si hubiesemos aceptado y vamos por la opcion 1
            AcceptAction();
        }
        else
        {
            //Si perdemos es como si hubiesemos declinado y vamos por la opcion 2
            DeclineAction();
        }
    }

    public void EndTransitionScene()
    {
        currentAction = currentAction.option1;
        StartNextEoEvent();
    }

    public void EndAdvanceScene()
    {
        if (currentAction.isNPCEvent)
        {
            angelController.gameObject.SetActive(false);
            gameOverController.gameObject.SetActive(false);
            actionController.gameObject.SetActive(false);
            transitionController.gameObject.SetActive(false);
            npcController.gameObject.SetActive(true);
            npcController.SetAction(currentAction);
        }
        else
        {
            angelController.gameObject.SetActive(false);
            gameOverController.gameObject.SetActive(false);
            npcController.gameObject.SetActive(false);
            transitionController.gameObject.SetActive(false);
            actionController.gameObject.SetActive(true);
            actionController.SetAction(currentAction);
        }
    }

    public void StartNextEoEvent()
    {
        //Si es un evento de FindObject
        if (currentAction.findObjectEvent != null && currentAction.findObjectEvent.GetType() == typeof(FindObjectEoEvent))
        {
            FindObjectEoEvent findObjectEoEvent = (FindObjectEoEvent)currentAction.findObjectEvent;

            //Si ya hemos cogido el objeto ponemos la siguiente esccena
            if (PlayerPrefs.HasKey(findObjectEoEvent.key))
            {
                currentAction = currentAction.option1;
                StartNextEoEvent();
                return;
            }
            else
            {
                if (currentAction.findObjectEvent.isNPCEvent)
                {
                    angelController.gameObject.SetActive(false);
                    gameOverController.gameObject.SetActive(false);
                    actionController.gameObject.SetActive(false);
                    transitionController.gameObject.SetActive(false);
                    npcController.gameObject.SetActive(true);
                    npcController.SetAction(currentAction.findObjectEvent);
                }
                else
                {
                    angelController.gameObject.SetActive(false);
                    gameOverController.gameObject.SetActive(false);
                    npcController.gameObject.SetActive(false);
                    transitionController.gameObject.SetActive(false);
                    actionController.gameObject.SetActive(true);
                    actionController.SetAction(currentAction.findObjectEvent);
                }
                currentAction = currentAction.findObjectEvent;
                return;
            }
        }

        if (currentAction.GetType() == typeof(AdvanceEoEvent))
        {
            npcController.gameObject.SetActive(false);
            actionController.gameObject.SetActive(false);
            gameOverController.gameObject.SetActive(false);
            angelController.gameObject.SetActive(false);
            transitionController.gameObject.SetActive(false);
            //Pintar el camino
            AdvanceEoEvent advEvent = (AdvanceEoEvent)currentAction;
            paintTripImage.SetFillAmount(advEvent.distance, advEvent.activeImage);
        }
        else if (currentAction.GetType() == typeof(MinigameEoEvent))
        {
            //Lanzar el minijuego y esperar a ver el resultado
            GameMgr.Instance.GetServer<SceneMgr>().PushScene(((MinigameEoEvent)currentAction).minigameScene);
            //SceneManager.LoadScene(((MinigameEoEvent)currentAction.option2).minigameScene);
        }
        else if (currentAction.GetType() == typeof(GameOverEoEvent))
        {
            if (PlayerPrefs.HasKey("Angel"))
            {
                if (PlayerPrefs.GetInt("Angel") == 1)
                {
                    npcController.gameObject.SetActive(false);
                    actionController.gameObject.SetActive(false);
                    gameOverController.gameObject.SetActive(false);
                    transitionController.gameObject.SetActive(false);
                    angelController.gameObject.SetActive(true);
                    angelController.SetAction(((GameOverEoEvent)currentAction).angelEvent);
                    currentAction = ((GameOverEoEvent)currentAction).angelEvent;
                    return;
                }
            }
            //TODO Lanzar el minijuego y esperar a ver el resultado
            npcController.gameObject.SetActive(false);
            actionController.gameObject.SetActive(false);
            angelController.gameObject.SetActive(false);
            transitionController.gameObject.SetActive(false);
            gameOverController.gameObject.SetActive(true);
            gameOverController.SetAction(currentAction);
        }
        else if (currentAction.GetType() == typeof(AdvanceGameOverEoEvent))
        {
            npcController.gameObject.SetActive(false);
            actionController.gameObject.SetActive(false);
            angelController.gameObject.SetActive(false);
            transitionController.gameObject.SetActive(false);
            gameOverController.gameObject.SetActive(true);
            gameOverController.SetAction(currentAction);
        }
        else if (currentAction.GetType() == typeof(TransitionEoEvent))
        {
            npcController.gameObject.SetActive(false);
            actionController.gameObject.SetActive(false);
            gameOverController.gameObject.SetActive(false);
            angelController.gameObject.SetActive(false);
            transitionController.gameObject.SetActive(true);
            transitionController.SetAction(currentAction);
        }else
        {
            if (currentAction.isNPCEvent)
            {
                angelController.gameObject.SetActive(false);
                gameOverController.gameObject.SetActive(false);
                actionController.gameObject.SetActive(false);
                transitionController.gameObject.SetActive(false);
                npcController.gameObject.SetActive(true);
                npcController.SetAction(currentAction);
            }
            else
            {
                angelController.gameObject.SetActive(false);
                gameOverController.gameObject.SetActive(false);
                npcController.gameObject.SetActive(false);
                transitionController.gameObject.SetActive(false);
                actionController.gameObject.SetActive(true);
                actionController.SetAction(currentAction);
            }
        }

        GameMgr.Instance.GetServer<AudioMgr>().Play(currentAction.clip);
    }
}
