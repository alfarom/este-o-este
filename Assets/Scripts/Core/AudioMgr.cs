﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioMgr : MonoBehaviour {

    public AudioSource audioSource;
    public AudioSource fxAudioSource;

    void Awake () {
        audioSource =  gameObject.AddComponent<AudioSource>();
        fxAudioSource = gameObject.AddComponent<AudioSource>();
    }
	
	public void PlayOneShot(AudioClip clip)
    {
        audioSource.Stop();
        audioSource.loop = false;
        audioSource.PlayOneShot(clip);
    }

    public void Play(AudioClip clip)
    {
        if (clip == null) return;
        audioSource.Stop();
        audioSource.clip = clip;
        audioSource.Play();
        audioSource.loop = true;
    }

    public void StopPlay()
    {
        audioSource.Stop();
    }

    public void StopPlayFX()
    {
        fxAudioSource.Stop();
    }

    public bool isPlaying()
    {
        return audioSource.isPlaying;
    }

    public void PlayFxOneShot(AudioClip clip)
    {
        fxAudioSource.Stop();
        fxAudioSource.PlayOneShot(clip);
    }

    public void PlayFX(AudioClip clip)
    {
        fxAudioSource.Stop();
        fxAudioSource.clip = clip;
        fxAudioSource.Play();
        fxAudioSource.loop = true;
    }

    public void SetMusicVolume(float volume)
    {
        audioSource.volume = volume;
    }

    public void SetFxVolume(float volume)
    {
        fxAudioSource.volume = volume;
    }


}
