﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MiguelController : MonoBehaviour {

    Rigidbody2D rb;
    Animator anim;
    [SerializeField]
    float speed = 10f;

	// Use this for initialization
	void Start () {
        rb = GetComponent<Rigidbody2D>();
        anim = GetComponent<Animator>();
	}
	
	// Update is called once per frame
	void Update ()
    {
        rb.velocity = new Vector2(Input.GetAxis("Horizontal") * speed, 0);
        anim.SetFloat("Speed", rb.velocity.magnitude);
	}
}
