﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "ScriptableObjects/Evento de minijuego")]
public class MinigameEoEvent : EoEvent
{
    [Tooltip("Nombre de la del minijuego")]
    public string minigameScene;

}
