﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "ScriptableObjects/Evento de avance (Game over)")]
public class AdvanceGameOverEoEvent : AdvanceEoEvent
{

    [Tooltip("Nombre de la escena final del juego")]
    public string nameScene;


    [Tooltip("Nombre del evento del ángel del juego")]
    public AngelEoEvent angelEvent;
}
