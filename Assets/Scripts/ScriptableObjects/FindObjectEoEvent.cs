﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "ScriptableObjects/Evento objeto encontrado")]
public class FindObjectEoEvent : EoEvent {


    [Tooltip("Nombre de la key para saber si el objeto ha sido cogido o no")]
    public string key;

}
