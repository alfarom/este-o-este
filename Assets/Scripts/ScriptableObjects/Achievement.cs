﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "ScriptableObjects/Logros")]
public class Achievement : ScriptableObject {

    [Tooltip("Icono del Logro")]
    public Sprite icon;

    [Tooltip("Icono del Logro en B/N")]
    public Sprite bwIcon;

    [Tooltip("Nombre del Logro")]
    public string achievmentName;

    [Tooltip("Descripción del Logro")]
    public string description;

    [Tooltip("Estado del logro (Desbloqueado o no)")]
    public bool achieved;
}
