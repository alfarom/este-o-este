﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "ScriptableObjects/Evento")]
public class EoEvent : ScriptableObject {

    [Tooltip("Identificador del evento")]
    public string id;

    [Tooltip("Texto que se mostrará en la pantalla")]
    public string text;

    [Tooltip("Texto que se mostrará en la opcion 1")]
    public string textOption1;

    [Tooltip("Evento que se va a desencadenar al aceptar la acción")]
    public EoEvent option1;

    [Tooltip("Texto que se mostrará en la opcion 2")]
    public string textOption2;

    [Tooltip("Evento que se va a desencadenar al rechazar la acción")]
    public EoEvent option2;

    [Tooltip("Define si es un evento para mostrar la imagen de un personaje o un fondo")]
    public bool isNPCEvent;

    [Tooltip("Imagen que se mostrará en el evento")]
    public Sprite image;

    [Tooltip("Música que debe empezar con esta tarjeta")]
    public AudioClip clip;

    [Tooltip("Sonido que se va a reproducir en el FX")]
    public AudioClip sfxClip;
    
    [Tooltip("Evento de encontrar objeto que salta si no es nulo")]
    public EoEvent findObjectEvent;
}
