﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "ScriptableObjects/Evento de transición")]
public class TransitionEoEvent : EoEvent {

    [Tooltip("Duracion de la escena")]
    public float duration = 0;

    [Tooltip("Si esta activado se hara un fundido de negro a la imagen")]
    public bool fadeTransition;
}
