﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "ScriptableObjects/Evento usar objeto ")]
public class UseObjectEoEvent : EoEvent
{
    [Tooltip("Nombre de la key para saber si el objeto ha sido cogido o no")]
    public string key;

    [Tooltip("Texto del boton que se va a mostrar")]
    public string optionText;

    [Tooltip("Evento al que te va a llevar si no tienes la llave")]
    public EoEvent alternativeEvent;
}
