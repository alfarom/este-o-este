﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "ScriptableObjects/Evento de avance")]
public class AdvanceEoEvent : EoEvent {

    [Tooltip("Distancia del camino que se va a mostrar")]
    [Range(0,1)]
    public float distance;

    [Tooltip("Nombre de la imagen que se va a activar")]
    public string activeImage;


}
