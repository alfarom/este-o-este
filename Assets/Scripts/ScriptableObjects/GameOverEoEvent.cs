﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "ScriptableObjects/Evento de gameover")]
public class GameOverEoEvent : EoEvent
{

    [Tooltip("Nombre de la escena final del juego")]
    public string nameScene;


    [Tooltip("Nombre del evento del ángel del juego")]
    public AngelEoEvent angelEvent;

}
