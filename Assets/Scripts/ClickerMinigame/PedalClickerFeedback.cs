﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PedalClickerFeedback : ClickerFeedback {

    [SerializeField]
    float aliensIniScale, aliensEndScale;
    [SerializeField]
    float pieIniRotation, pieEndRotation;
    [SerializeField]
    Transform pieTransform, ovniTransform;

    
    public override void Start()
    {
        base.Start();
    }
    public override void UpdateFeedbackClickerPedal(float currentDist, float goalDist, float currentSpeed)
    {
        float percentageDist = currentDist / goalDist;
        pieTransform.rotation = Quaternion.Euler(0, 0, Mathf.Lerp(pieIniRotation, pieEndRotation, ((currentSpeed + 10)/30)));
        ovniTransform.localScale = new Vector3(Mathf.Lerp(aliensIniScale, aliensEndScale, percentageDist), Mathf.Lerp(aliensIniScale, aliensEndScale, percentageDist), 0);
        //birraContent.position = Vector3.Lerp(birraStartPos.position, birraEndPos.position, percentageBirra);
    }
    public override void UpdateFeedbackClicker(int currentClicks, int goalClicks)
    {
        float percentageClicks = (float)currentClicks / (float)goalClicks;
        pieTransform.rotation = Quaternion.Euler(0, 0, pieTransform.rotation.eulerAngles.z + Mathf.Lerp(pieIniRotation, pieEndRotation, (1 / (float)goalClicks)));
        ovniTransform.localScale = new Vector3(Mathf.Lerp(aliensIniScale, aliensEndScale, percentageClicks), Mathf.Lerp(aliensIniScale, aliensEndScale, percentageClicks), 0);
        //birraContent.position = Vector3.Lerp(birraStartPos.position, birraEndPos.position, percentageBirra);
    }

    public override void UpdateTimeClicker(float currentTime, float goalTime)
    {
        base.UpdateTimeClicker(currentTime, goalTime);
        pieTransform.rotation = Quaternion.Euler(0, 0, Mathf.Clamp(pieTransform.rotation.eulerAngles.z - Time.deltaTime * 6, 0, pieEndRotation));

    }

    IEnumerator GoBackAnimation()
    {
        float deltaTime = 0;
        while(deltaTime<3)
        {
            deltaTime += Time.deltaTime;
            pieTransform.rotation = Quaternion.Euler(0, 0, Mathf.Clamp(pieTransform.rotation.eulerAngles.z - Time.deltaTime * 6, 0, pieEndRotation));
            ovniTransform.localScale = new Vector3(Mathf.Lerp(aliensEndScale, aliensIniScale, deltaTime/3), Mathf.Lerp(aliensEndScale, aliensIniScale, deltaTime / 3), 0);
            yield return null;
        }
    }

    public override void LaunchGameStart()
    {
        StartCoroutine(StartGameAnimation());
    }

    IEnumerator StartGameAnimation()
    {
        pieTransform.rotation = Quaternion.Euler(0, 0, pieIniRotation);
        ovniTransform.localScale = new Vector3(aliensIniScale, aliensIniScale, 0);
        textM.text = "NO DEJES QUE SE ESCAPEN LOS ALIENS";
        textAnimator.SetTrigger("TextIn");
        yield return new WaitForSeconds(2f);
        textM.text = "3";
        yield return new WaitForSeconds(1f);
        textM.text = "2";
        //birraAnim.SetTrigger("DentroBirra");
        yield return new WaitForSeconds(1f);
        textM.text = "1";
        yield return new WaitForSeconds(1f);
        textAnimator.SetTrigger("TextOut");
        textM.text = "DALE MÁS GAS";
        clickerManager.StartGame();
    }

    public override void LaunchRoundEnd(bool last)
    {
        StartCoroutine(RoundEndAnimation(last));
    }

    IEnumerator RoundEndAnimation(bool last)
    {
        StartCoroutine(GoBackAnimation());
        yield return new WaitForSeconds(1f);
        //birraContent.position = birraStartPos.position;
        textM.text = last ? "¡YA CASI ESTÁS!" : "¡SIGUE CONDUCIENDO!";
        textAnimator.SetTrigger("TextIn");
        yield return new WaitForSeconds(1.5f);
        textM.text = "3";
        yield return new WaitForSeconds(1f);
        textM.text = "2";
        //birraAnim.SetTrigger("DentroBirra");
        yield return new WaitForSeconds(1f);
        textM.text = "1";
        yield return new WaitForSeconds(1f);
        textAnimator.SetTrigger("TextOut");
        textM.text = "YA";
        clickerManager.StartNewRound();
    }

    public override void LaunchGameEnd()
    {
        StartCoroutine(GameEndAnimation());
    }

    IEnumerator GameEndAnimation()
    {
        //birraAnim.SetTrigger("FueraBirra");
        yield return new WaitForSeconds(2f);
        clickerManager.WinGame();
    }

    public override void LaunchTimeUp()
    {
        StartCoroutine(TimeUpAnimation());
    }

    IEnumerator TimeUpAnimation()
    {
        clickerManager.LoseGame();
        yield return null;
    }
}
