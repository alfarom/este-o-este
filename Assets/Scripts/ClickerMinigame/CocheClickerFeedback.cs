﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CocheClickerFeedback : ClickerFeedback
{


    [SerializeField]
    Sprite palancaFirst;

    [SerializeField]
    SpriteRenderer palancaRenderer;

    [SerializeField]
    Transform cocheTransform, barraTransform, altBarraTransform;
    [SerializeField]
    float [] cocheRotations, barraScales, secondBarraScales;

    int round = 1;


    public override void Start()
    {
        base.Start();
        if (Random.Range(0, 1f) > 0.1f)
            altBarraTransform.gameObject.SetActive(false);

    }

    public override void UpdateFeedbackClicker(int currentClicks, int goalClicks)
    {
        float percentageBirra = (float)currentClicks / (float)goalClicks;
        cocheTransform.rotation = Quaternion.Euler(0, 0, Mathf.Lerp(cocheRotations[round - 1], cocheRotations[round], percentageBirra));
        barraTransform.localScale = new Vector3(barraTransform.localScale.x, Mathf.Lerp(barraScales[round - 1], barraScales[round], percentageBirra), barraTransform.localScale.z);
        altBarraTransform.localScale = new Vector3(altBarraTransform.localScale.x, Mathf.Lerp(secondBarraScales[round - 1], secondBarraScales[round], percentageBirra), altBarraTransform.localScale.z);
    }

    public override void LaunchGameStart()
    {
        StartCoroutine(StartGameAnimation());
    }

    IEnumerator StartGameAnimation()
    {
        textM.text = "LEVANTA EL COCHE PARA CAMBIAR LA RUEDA";
        textAnimator.SetTrigger("TextIn");
        yield return new WaitForSeconds(2f);
        textM.text = "3";
        yield return new WaitForSeconds(1f);
        textM.text = "2";
        //birraAnim.SetTrigger("DentroBirra");
        yield return new WaitForSeconds(1f);
        textM.text = "1";
        yield return new WaitForSeconds(1f);
        textAnimator.SetTrigger("TextOut");
        textM.text = "YA";
        clickerManager.StartGame();
    }

    public override void LaunchRoundEnd(bool last)
    {
        StartCoroutine(RoundEndAnimation(last));
    }

    IEnumerator RoundEndAnimation(bool last)
    {
        //birraAnim.SetTrigger("FueraBirra");
        round++;
        palancaRenderer.sprite = palancaFirst;
        yield return new WaitForSeconds(1f);
        textM.text = last ? "TODAVÍA NO ESTÁ SUFICIENTEMENTE ARRIBA" : "SIGUE SUBIENDO";
        textAnimator.SetTrigger("TextIn");
        yield return new WaitForSeconds(1.5f);
        textM.text = "3";
        yield return new WaitForSeconds(1f);
        textM.text = "2";
        //birraAnim.SetTrigger("DentroBirra");
        yield return new WaitForSeconds(1f);
        textM.text = "1";
        yield return new WaitForSeconds(1f);
        textAnimator.SetTrigger("TextOut");
        textM.text = "YA";
        clickerManager.StartNewRound();
    }

    public override void LaunchGameEnd()
    {
        StartCoroutine(GameEndAnimation());
    }

    IEnumerator GameEndAnimation()
    {
        //birraAnim.SetTrigger("FueraBirra");
        yield return new WaitForSeconds(2f);
        clickerManager.WinGame();
    }

    public override void LaunchTimeUp()
    {
        StartCoroutine(TimeUpAnimation());
    }

    IEnumerator TimeUpAnimation()
    {
        clickerManager.LoseGame();
        yield return null;
    }
}
