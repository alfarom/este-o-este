﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HachazoClickerFeedback : ClickerFeedback
{

    [SerializeField]
    Sprite hachazoFirst;

    [SerializeField]
    SpriteRenderer hachazoRenderer;

    [SerializeField]
    SpriteRenderer puertaRenderer;
    [SerializeField]
    Sprite[] puertaSprites;

    int round = 0;


    public override void Start()
    {
        base.Start();

    }
    public override void LaunchGameStart()
    {
        StartCoroutine(StartGameAnimation());
    }

    IEnumerator StartGameAnimation()
    {
        textM.text = "ESTA PUERTA NO SE ABRIRÁ DE OTRA FORMA";
        textAnimator.SetTrigger("TextIn");
        yield return new WaitForSeconds(2f);
        textM.text = "3";
        yield return new WaitForSeconds(1f);
        textM.text = "2";
        //birraAnim.SetTrigger("DentroBirra");
        yield return new WaitForSeconds(1f);
        textM.text = "1";
        yield return new WaitForSeconds(1f);
        textAnimator.SetTrigger("TextOut");
        textM.text = "YA";
        clickerManager.StartGame();
    }

    public override void LaunchRoundEnd(bool last)
    {
        StartCoroutine(RoundEndAnimation(last));
    }

    IEnumerator RoundEndAnimation(bool last)
    {
        //birraAnim.SetTrigger("FueraBirra");
        round++;
        puertaRenderer.sprite = puertaSprites[round];
        hachazoRenderer.sprite = hachazoFirst;
        yield return new WaitForSeconds(1f);
        textM.text = last ? "¡UNO MÁS PARA ENTRAR!" : "¡DESTROZA ESA PUERTA!";
        textAnimator.SetTrigger("TextIn");
        yield return new WaitForSeconds(1.5f);
        textM.text = "3";
        yield return new WaitForSeconds(1f);
        textM.text = "2";
        //birraAnim.SetTrigger("DentroBirra");
        yield return new WaitForSeconds(1f);
        textM.text = "1";
        yield return new WaitForSeconds(1f);
        textAnimator.SetTrigger("TextOut");
        textM.text = "YA";
        clickerManager.StartNewRound();
    }

    public override void LaunchGameEnd()
    {
        StartCoroutine(GameEndAnimation());
    }

    IEnumerator GameEndAnimation()
    {
        puertaRenderer.sprite = puertaSprites[++round];
        clickerManager.EndRound();
        yield return new WaitForSeconds(2f);
        clickerManager.WinGame();
    }

    public override void LaunchTimeUp()
    {
        StartCoroutine(TimeUpAnimation());
    }

    IEnumerator TimeUpAnimation()
    {
        clickerManager.LoseGame();
        yield return null;
    }
}
