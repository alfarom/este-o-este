﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PedalClickerManager : ClickerManager {


    [System.Serializable]
    public class PedalRound
    {
        public float goalClicks;
        public float goalTime;
    };
        
    [SerializeField]
    PedalRound[] clickRounds;
    [SerializeField]
    float maxSpeed = 20f;
    [SerializeField]
    EoEvent historiaAlien;


    float gasPedal = 0;
    float currentDistance = 0;


    public override void CallFeedbackGameStart()
    {
        if (clickerFeedback != null)
            clickerFeedback.LaunchGameStart();
        else
            StartGame();
    }
    public override void CallFeedbackGameEnd()
    {
        if (clickerFeedback != null)
            clickerFeedback.LaunchGameEnd();
        else
            WinGame();

    }
    public override void CallFeedbackTimeUp()
    {
        if (clickerFeedback != null)
            clickerFeedback.LaunchTimeUp();
        else
            LoseGame();

    }
    public override void CallFeedbackRoundStart()
    {
        if (clickerFeedback != null)
            clickerFeedback.LaunchRoundEnd((currentRound == clickRounds.Length - 1));
        else
            StartNewRound();

    }
    public override void CallFeedbackRoundEnd()
    {
        if (clickerFeedback != null)
            clickerFeedback.LaunchRoundEnd((currentRound == clickRounds.Length - 1));
    }

    public override void StartGame()
    {
        currentRound = 0;
        roundEnded = false;
        roundCoroutine = StartCoroutine(ClickerRound());
    }

    IEnumerator ClickerRound()
    {
        while (!roundEnded)
        {
            currentTime += Time.deltaTime;
            clickerFeedback.UpdateTimeClicker(currentTime, clickRounds[currentRound].goalTime);

            if (Input.GetMouseButtonDown(0))
            {
                gasPedal++; 
                CheckGameState();
            }
            gasPedal -= Time.deltaTime*4;
            gasPedal = Mathf.Clamp(gasPedal, -10, 20);
            currentDistance += gasPedal*Time.deltaTime;
            currentDistance = Mathf.Clamp(currentDistance, 0, clickRounds[currentRound].goalClicks);
            clickerFeedback.UpdateFeedbackClickerPedal(currentDistance, clickRounds[currentRound].goalClicks, gasPedal);
            if (currentTime >= clickRounds[currentRound].goalTime)
            {
                TimeUp();
                CallFeedbackTimeUp();
            }
            yield return null;
        }
    }

    public override void TimeUp()
    {
        LoseRound();
    }

    public override void CheckGameState()
    {
        if (currentDistance == clickRounds[currentRound].goalClicks)
        {
            StopCoroutine(roundCoroutine);
            CheckNextRound();
        }
    }
    public override void EndRound()
    {
        clickerFeedback.StopTimeClicker();
        roundEnded = true;
    }
    public override void CheckNextRound()
    {
        if (currentRound == clickRounds.Length - 1)
        {
            CallFeedbackGameEnd();
        }
        else
        {
            EndRound();
            currentRound++;
            CallFeedbackRoundStart();
        }
    }

    public override void StartNewRound()
    {
        currentTime = 0;
        currentDistance = 0;
        gasPedal = 0;
        roundEnded = false;
        roundCoroutine = StartCoroutine(ClickerRound());
    }

    public override void LoseRound()
    {
        roundEnded = true;
    }

    public override void WinGame()
    {
        GameMgr.Instance.GetEoEventsMgr().SetMiniGameSuccess(true);
        GameMgr.Instance.GetServer<SceneMgr>().ReturnScene(true);
    }

    public override void LoseGame()
    {
        //    GameMgr.Instance.GetEoEventsMgr().SetMiniGameSuccess(false);
        //    GameMgr.Instance.GetServer<SceneMgr>().ReturnScene(true);
        GameMgr.Instance.InitialEoEvent = historiaAlien;
        GameMgr.Instance.GetServer<SceneMgr>().ChangeScene("MainScene");
    }
}
