﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BirraClickerFeedback : ClickerFeedback
{
    [SerializeField]
    protected Animator birraAnim;

    [SerializeField]
    Transform birraContent, birraStartPos, birraEndPos;



    public override void Start()
    {
        base.Start();
    }

    public override void UpdateFeedbackClicker(int currentClicks, int goalClicks)
    {
        float percentageBirra = (float)currentClicks / (float)goalClicks;
        birraContent.position = Vector3.Lerp(birraStartPos.position, birraEndPos.position, percentageBirra);
    }

    public override void LaunchGameStart()
    {
        StartCoroutine(StartGameAnimation());
    }

    IEnumerator StartGameAnimation()
    {
        textM.text = "¿PREPARADO PARA BEBER BIRRA?";
        textAnimator.SetTrigger("TextIn");
        yield return new WaitForSeconds(2f);
        textM.text = "3";
        yield return new WaitForSeconds(1f);
        textM.text = "2";
        birraAnim.SetTrigger("DentroBirra");
        yield return new WaitForSeconds(1f);
        textM.text = "1";
        yield return new WaitForSeconds(1f);
        textAnimator.SetTrigger("TextOut");
        textM.text = "YA";
        clickerManager.StartGame();
    }

    public override void LaunchRoundEnd(bool last)
    {
        StartCoroutine(RoundEndAnimation(last));
    }

    IEnumerator RoundEndAnimation(bool last)
    {
        birraAnim.SetTrigger("FueraBirra");
        yield return new WaitForSeconds(1f);
        birraContent.position = birraStartPos.position;
        textM.text = last?"¡ÚLTIMA RONDA!": "¡OTRA RONDA!";
        textAnimator.SetTrigger("TextIn");
        yield return new WaitForSeconds(1.5f);
        textM.text = "3";
        yield return new WaitForSeconds(1f);
        textM.text = "2";
        birraAnim.SetTrigger("DentroBirra");
        yield return new WaitForSeconds(1f);
        textM.text = "1";
        yield return new WaitForSeconds(1f);
        textAnimator.SetTrigger("TextOut");
        textM.text = "YA";
        clickerManager.StartNewRound();
    }

    public override void LaunchGameEnd()
    {
        StartCoroutine(GameEndAnimation());
    }

    IEnumerator GameEndAnimation()
    {
        birraAnim.SetTrigger("FueraBirra");
        yield return new WaitForSeconds(2f);
        clickerManager.WinGame();
    }

    public override void LaunchTimeUp()
    {
        StartCoroutine(TimeUpAnimation());
    }

    IEnumerator TimeUpAnimation()
    {
        clickerManager.LoseGame();
        yield return null;
    }
}
