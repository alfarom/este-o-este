﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class ClickerManager : MonoBehaviour {


    [System.Serializable]
    public class Round
    {
        public int goalClicks;
        public float goalTime;
    };



    /// <summary>
    /// DEBUG TEXTS
    /// </summary>
    public TextMeshProUGUI titleTextMesh;

    [SerializeField]
    protected Round[] rounds;


    protected ClickerFeedback clickerFeedback;
    protected int currentRound = 0;
    protected int currentClicks = 0;
    protected float currentTime = 0;
    protected bool gameEnded = false;
    protected bool gameStarted = false;
    protected bool roundEnded = true;
    protected Coroutine roundCoroutine;

    public virtual void Start()
    {
        clickerFeedback = GetComponent<ClickerFeedback>();
        //numbertextM.text = currentClicks.ToString() + " / " + rounds[currentRound].goalClicks.ToString();
        CallFeedbackGameStart();
    }

    public virtual void CallFeedbackGameStart()
    {
        if (clickerFeedback != null)
            clickerFeedback.LaunchGameStart();
        else
            StartGame();
    }
    public virtual void CallFeedbackGameEnd()
    {
        if (clickerFeedback != null)
            clickerFeedback.LaunchGameEnd();
        else
            WinGame();

    }
    public virtual void CallFeedbackTimeUp()
    {
        if (clickerFeedback != null)
            clickerFeedback.LaunchTimeUp();
        else
            LoseGame();

    }
    public virtual void CallFeedbackRoundStart()
    {
        if (clickerFeedback != null)
            clickerFeedback.LaunchRoundEnd((currentRound == rounds.Length-1));
        else
            StartNewRound();

    }
    public virtual void CallFeedbackRoundEnd()
    {
        if (clickerFeedback != null)
            clickerFeedback.LaunchRoundEnd((currentRound == rounds.Length - 1));
    }

    public virtual void StartGame()
    {
        currentRound = 0;
        roundEnded = false;
        //titletextM.text = "ROUND " + currentRound;
        roundCoroutine = StartCoroutine(ClickerRound());
    }

    protected virtual IEnumerator ClickerRound()
    {
        while(!roundEnded)
        {
            currentTime += Time.deltaTime;
            clickerFeedback.UpdateTimeClicker(currentTime, rounds[currentRound].goalTime);
            //subtitletextM.text = currentTime.ToString("F0") + " / " + rounds[currentRound].goalTime.ToString("F0");
            if (Input.GetMouseButtonDown(0))
            {
                currentClicks++;
                // Audio
                switch(Random.Range(0,3))
                {
                    case 0:
                        GetComponent<AudioSource>().clip = Resources.Load<AudioClip>("Music/SFX/beber1");
                        break;
                    case 1:
                        GetComponent<AudioSource>().clip = Resources.Load<AudioClip>("Music/SFX/beber2");
                        break;
                    case 2:
                        GetComponent<AudioSource>().clip = Resources.Load<AudioClip>("Music/SFX/beber3");
                        break;
                }
                GetComponent<AudioSource>().Play();
            
                clickerFeedback.UpdateFeedbackClicker(currentClicks, rounds[currentRound].goalClicks);
                CheckGameState();
            }
            if (currentTime >= rounds[currentRound].goalTime)
            {
                TimeUp();
                CallFeedbackTimeUp();
            }
            yield return null;
        }
    }

    public virtual void TimeUp()
    {
        LoseRound();
    }

    public virtual void CheckGameState()
    {
        //numbertextM.text = currentClicks.ToString() + " / " + rounds[currentRound].goalClicks.ToString();
        if (currentClicks == rounds[currentRound].goalClicks)
        {
            StopCoroutine(roundCoroutine);
            CheckNextRound();
        }
    }

    public virtual void EndRound()
    {
        clickerFeedback.StopTimeClicker();
        roundEnded = true;
    }

    public virtual void CheckNextRound()
    {
        if(currentRound == rounds.Length-1)
        {
            CallFeedbackGameEnd();
        }
        else
        {
            EndRound();
            currentRound++;
            CallFeedbackRoundStart();
        }
    }

    public virtual void StartNewRound()
    {
        currentTime = 0;
        currentClicks = 0;
        roundEnded = false;
        roundCoroutine = StartCoroutine(ClickerRound());
    }

    public virtual void LoseRound()
    {
        roundEnded = true;
    }

    public virtual void WinGame()
    {
        GameMgr.Instance.GetEoEventsMgr().SetMiniGameSuccess(true);
        GameMgr.Instance.GetServer<SceneMgr>().ReturnScene(true);
    }

    public virtual void LoseGame()
    {
        GameMgr.Instance.GetEoEventsMgr().SetMiniGameSuccess(false);
        GameMgr.Instance.GetServer<SceneMgr>().ReturnScene(true);
    }
}
