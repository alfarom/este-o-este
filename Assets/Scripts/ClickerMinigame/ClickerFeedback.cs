﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class ClickerFeedback : MonoBehaviour
{

    protected ClickerManager clickerManager;
    [SerializeField]
    protected Color quesitoStartColor, quesitoEndColor;
    [SerializeField]
    protected Image quesitoImage;
    
    [SerializeField]
    protected TextMeshProUGUI textM;
    [SerializeField]
    protected Animator textAnimator;

    public virtual void Start()
    {
        clickerManager = GetComponent<ClickerManager>();
    }

    public virtual void UpdateFeedbackClicker(int currentClicks, int goalClicks) { }
    public virtual void UpdateFeedbackClickerPedal(float currentDist, float goalDist, float currentSpeed) { }

    public virtual void UpdateTimeClicker(float currentTime, float goalTime)
    {
        quesitoImage.fillAmount = 1 - currentTime / goalTime;
        quesitoImage.color = Color.Lerp(quesitoStartColor, quesitoEndColor, currentTime / goalTime);
    }
    public virtual void StopTimeClicker()
    {
        quesitoImage.color = Color.clear;
    }
    public virtual void LaunchGameStart() { }
    public virtual void LaunchGameEnd() { }
    public virtual void LaunchTimeUp() { }
    public virtual void LaunchRoundStart() { }
    public virtual void LaunchRoundEnd(bool start) { }


}
