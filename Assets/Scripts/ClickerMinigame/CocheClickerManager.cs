﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CocheClickerManager : ClickerManager {


    [SerializeField]
    Sprite palancaFirst, palancaSecond;

    [SerializeField]
    SpriteRenderer palancaRenderer;

    bool palanquica = false;

    void Update()
    {
        if(!roundEnded)
        {
            if (Input.GetMouseButtonDown(0))
            {
                palanquica = !palanquica;

                palancaRenderer.sprite = palanquica ? palancaFirst : palancaSecond;
            }
        }
    }

    protected override IEnumerator ClickerRound()
    {
        while (!roundEnded)
        {
            currentTime += Time.deltaTime;
            clickerFeedback.UpdateTimeClicker(currentTime, rounds[currentRound].goalTime);
            //subtitleText.text = currentTime.ToString("F0") + " / " + rounds[currentRound].goalTime.ToString("F0");
            if (Input.GetMouseButtonDown(0))
            {
                currentClicks++;
                // Audio
                switch (Random.Range(0, 2))
                {
                    case 0:
                        GetComponent<AudioSource>().clip = Resources.Load<AudioClip>("Music/SFX/palancote1");
                        break;
                    case 1:
                        GetComponent<AudioSource>().clip = Resources.Load<AudioClip>("Music/SFX/palancote2");
                        break;
                }
                GetComponent<AudioSource>().Play();

                clickerFeedback.UpdateFeedbackClicker(currentClicks, rounds[currentRound].goalClicks);
                CheckGameState();
            }
            if (currentTime >= rounds[currentRound].goalTime)
            {
                TimeUp();
                CallFeedbackTimeUp();
            }
            yield return null;
        }
    }

}
